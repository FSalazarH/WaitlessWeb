$(function() {
    var form = $('#init-chat');
    var method = form.attr('method');;
    var url = form.attr('action');;
    var formMessages = $('#form-messages');
    var name;
    var rol;
    var phone;
    var htmlResponse;
    
    $(form).on('submit', function(e) {    
        e.preventDefault();
        
        if (!validateName(name) || !validateRol(rol) || !validatePhone(phone)) {
            return;
        } 

        $.ajax({
            beforeSend: function (xhr) {
                $('#form').slideUp();
                $('#iphone').val("56" + phone);
                formMessages.show();
                formMessages.html('<strong>por favor espere</strong>');                                
            },
            type: method,
            url: url,
            data: $(this).serialize(),
            dataType: "json",
            crossDomain: true,
            xhrFields: {
                withCredentials: false
            },
            success: function (data) {
                if ((data.status).localeCompare("ok")) {
                    htmlResponse = '<p></p>';
                } else if ((data.status).localeCompare("warning")) {
                    htmlResponse = '<p>Se va a reestablecer el chat</p>';
                } else if ((data.status).localeCompare("error")) {
                    htmlResponse = '<p>Error al iniciar Chat</p>';
                } else {
                    htmlResponse = '<p>Error al realizar conexión</p>';
                }
                formMessages.html(htmlResponse);
            },
            error: function (jqXHR, status, error) {
                formMessages.html('<p>Error al realizar conexión</p>');
                console.log(status);
                console.log(error);
            },
            complete: function (jqXHR, status) {
                console.log(status);
            },
            timeout: 30000
        });
    });
    
    $('#iname').on('change', function () {
        name = $('#iname').val();
        if (!validateName(name)) {
            $('#iname').parent().addClass('has-error');
            return;
        }
    });
    
    $('#irol').on('change', function () {
        rol = $('#irol').val();
        if (!validateRol(rol)) {
            $('#irol').parent().addClass('has-error');
            return;
        } 
    });
    
    $('#iphone').on('change', function () {
        phone = $('#iphone').val();
        if (!validatePhone(phone)) {
            $('#iphone').parent().addClass('has-error');
            return;
        } 
    });
    
    function validateRol(number) {
        var digv;
        var pos = number.indexOf('-');
        
        if (number === '' || number.length < 2) {
            console.log('rol demasiado corto');
            return false;
        }
        
        if (pos === -1) {
            var d = number[number.length - 1];
            var formated = number.substring(0, number.length - 1) + "-" + d;
            $('#irol').val(formated);
            return validateRol(formated);
        }
        
        digv = parseInt(number[pos+1]);
        number = number.substring(0, pos);
        
        if (number.length < 8 || number.length > 9) {
            $('#irol').parent().addClass('has-error');
            console.log('rol demasiado corto o largo');
            return false;
        }
        
        for(var i = 0; i < number.length; i++){
            if (!/^([0-9])*$/.test(number)){
                console.log("fail");
            }
        }
        
        var rolstring = rol.split('');
        var sum = parseInt(rolstring[8]) * 2 
                + parseInt(rolstring[7]) * 3
                + parseInt(rolstring[6]) * 4
                + parseInt(rolstring[5]) * 5
                + parseInt(rolstring[4]) * 6
                + parseInt(rolstring[3]) * 7
                + parseInt(rolstring[2]) * 2
                + parseInt(rolstring[1]) * 3
                + parseInt(rolstring[0]) * 4;
        var div = (sum % 11);
        if (div !== 0){
            div = 11 - div;
        }
        if (div === digv){
          $('#irol').parent().removeClass('has-error');
          $('#irol').parent().addClass('has-success');
          return true;
        }
        
        $('#irol').parent().addClass('has-error');
        return false;
    }
    
    function validatePhone(number) {
        if ((!/^([0-9])*$/.test(number))||(!(/^\d{9}$/.test(number)))) {
            $('#phone').parent().addClass('has-error');
            return false;
        }
        $('#iphone').parent().removeClass('has-error');
        $('#iphone').parent().addClass('has-success');
        return true; 
    }
    
    function validateName(name) {
        if (name.length < 3) {
            return false;
        }
        $('#iname').parent().removeClass('has-error');
        $('#iname').parent().addClass('has-success');
        return true;
    }
});

