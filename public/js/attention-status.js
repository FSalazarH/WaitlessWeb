$(function() {
    $('.attentionStatus').bootstrapToggle({
      on: 'Disponible',
      off: 'Deshabilitada'
    });
    
    $('.attentionStatus').each(function (i) {
        var status = $(this).data('status');
        if (status === "disable") {
            $(this).bootstrapToggle('off');
        } else if (status === "enable") {
            $(this).bootstrapToggle('on');
        }
    });
    
    $('.attentionStatus').on('change', function () {
        var id = $(this).data('id');
        var status = $(this).data('status');
        var tmp;
        
        if (status === "disable") {
            $(this).val('enable');
        } else if (status === "enable") {
            tmp = $('.formStatus[data-id=' + id + ']').find('input[data-id=' + id + ']');
            tmp.val("disable");
        }
        
        $('.formStatus[data-id=' + id + ']').submit();
    });
});