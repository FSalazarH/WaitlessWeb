<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
      <h4>@yield('navbar-title')</h4>
  </div>
</nav>