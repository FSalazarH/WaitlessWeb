<!DOCTYPE html>
<html lang="es">
  <head>

    @include('globals.head')
    
    @include('globals.styles')
    
    @stack('styles')

  </head>
  <body>
    
    @yield('navbar')
	
    @yield('content')
    
    @yield('modal')
    
    @include('globals.scripts')
    
    @stack('scripts')
    
  </body>
</html>
