@extends ('globals.layout')

@push('styles')
<link href="{{ asset('css/admin.css') }}" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endpush

@section ('navbar')
    @include ('admins.partials.navbar')    
@endsection

@section ('content')
<div class="container main-container ">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <h2>Colas Disponibles <a href="{{ route('admin.queues.create') }}" class="btn btn-primary text-uppercase create-queue">Crear Cola</a></h2> 
      <ul class="list-group">
        @foreach ($queues as $index => $queue)
        <li class="list-group-item">
          <div class="row">
            <div class="col-md-9">
              <h4 class="list-group-item-heading">{{ $index+1 }} - {{ $queue->name }}</h4>
            </div>
            <div class="col-md-3"> 
              {!! Form::open(['route' => ['admin.queues.destroy', $queue->id], 'method' => 'delete']) !!}
                <a href="{{ route('admin.queues.edit', ['id' => $queue->id]) }}" class="btn btn-default btn-sm">Editar</a>
                <button type="submit" class="btn btn-warning btn-sm delete-btn">Eliminar</button>
              {!! Form::close() !!}
            </div>
          </div>
          <h5>Estado de Atención</h5>
          {!! Form::open(['route' => ['admin.queues.update', $queue->id], 'method' => 'put', 'class' => 'formStatus', 'data-id' => $queue->id]) !!}
            <input type="checkbox" name="isenable" data-status="{{ $queue->status }}" data-id="{{ $queue->id }}" class="attentionStatus">
            <input type='hidden' name="disable" data-id="{{ $queue->id }}">
          {!! Form::close() !!}
        </li>
        @endforeach
      </ul>
    </div>
  </div>
</div>
@endsection

@section('modal')
<div class="modal fade confirmDelete" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteLabel">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body"><h4>¿Está seguro que desea Eliminar este recurso?</h4></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-warning">Eliminar</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="{{ asset('js/delete-modal.js') }}"></script>
<script src="{{ asset('js/attention-status.js') }}"></script>
@endpush