<h1>Admin Login</h1>
<form role="form" method="POST" action="{{ url('admin/login') }}">
    {!! csrf_field() !!}
    <div class="form-group">
      <label for="">Email</label>
      <input type="email" name="email" class="form-control" value="{{ old('email')}}" id="" placeholder="">
    </div>
    <div class="form-group">
      <label for="">Password</label>
      <input type="password" class="form-control" name="password" id="" placeholder="">
    </div>
    <button type="submit" name="submit-button" class="btn btn-sm btn-default btn-block">Entrar</button>
</form>