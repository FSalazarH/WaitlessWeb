@extends ('globals.layout')

@push('styles')
  <link href="{{ asset('css/main.css') }}" rel="stylesheet">
@endpush

@section ('title')

@section ('content')

<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <img src="{{ asset('img/waitless_logo.png') }}" class="center-block logo-form">
      <div id="form">
        <form id="init-chat" method="post" class="form-horizontal" action="https://api.waitless.zeek.cl/whatsapp/sessions">
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <div class="form-group">
            <div class="col-md-6 col-md-offset-3">
              <input id="iname" name="name" type="text" placeholder="Nombre" class="form-control" required>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-3">
              <input id="irol" name="rol" type="text" class="form-control" placeholder="ROL USM" required>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-3">
              <div class="row">
                <div class="col-md-3">
                  <select class="form-control">
                    <option>+56</option>
                  </select>
                </div>
                <div class="col-md-9">
                  <input id="iphone" name="phone" type="text" placeholder="Nº de Telefono" class="form-control" required> 
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12 text-center">
              <button type="submit" class="btn btn-primary text-uppercase">Iniciar Chat</button>
            </div>
          </div>
        </form>
      </div>
      <div class="row">
          <div class="col-md-6 col-md-offset-3">
              <div class="alert alert-info center-block" role="alert" id="form-messages"></div>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push ('scripts')
<script src="{{ asset('js/form-whatsappbot.js') }}"></script>
@endpush

