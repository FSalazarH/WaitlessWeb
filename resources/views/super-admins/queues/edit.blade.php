<h1>Editar Cola</h1>

{!! Form::open(['route' => ['super-admin.queues.update', $queue->id], 'method' => 'put']) !!}
    <label>Nombre</label>
    <input type="text" name="name" value="{{ $queue->name }}">
    <button type="submit">Editar</button>
{!! Form::close() !!}