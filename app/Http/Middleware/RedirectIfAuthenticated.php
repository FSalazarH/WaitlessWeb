<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('super_admin')->check()) {
            return redirect('/super-admin');
        } elseif (Auth::guard('admin')->check()) {
            return redirect('/admin');
        } elseif (Auth::guard()->check()) {
            return redirect('/');
        }

        return $next($request);
    }
}
