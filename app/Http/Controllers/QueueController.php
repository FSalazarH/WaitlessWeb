<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Queue;

class QueueController extends Controller
{
    public function create()
    {
        if (auth()->guard('super_admin')->check()) {
            return view('super-admins.queues.create');
        } else if (auth()->guard('admin')->check()) {
            return view('admins.queues.create');
        }
    }
    
    public function store(Request $request)
    {
        $queue = new Queue();
        $queue->name = $request->name;
        $queue->save();
        
        if (auth()->guard('super_admin')->check()) {
            return redirect('super-admin');
        } else if (auth()->guard('admin')->check()) {
            return redirect('admin');
        }
    }
    
    public function edit($id)
    {
        $queue = Queue::find($id);
        if (auth()->guard('super_admin')->check()) {
            return view('super-admins.queues.edit', ['queue' => $queue]);
        } else if (auth()->guard('admin')->check()) {
            return view('admins.queues.edit', ['queue' => $queue]);
        }
    }
    
    public function update($id, Request $request)
    {
        $queue = Queue::find($id);
        
        if ($request->has('name')) {
            $queue->name = $request->name;
        }
        
        if ($request->has('status')) {
            $queue->status = $request->status;
        } else if ($request->has('isenable')) {
            $queue->status = $request->isenable;
        } else if ($request->has('disable')) {
            $queue->status = $request->disable;
        }
        $queue->save();
        
        if (auth()->guard('super_admin')->check()) {
            return redirect('super-admin');
        } else if (auth()->guard('admin')->check()) {
            return redirect('admin');
        }
    }
    
    public function destroy($id)
    {
        Queue::destroy($id);
        if (auth()->guard('super_admin')->check()) {
            return redirect('super-admin');
        } else if (auth()->guard('admin')->check()) {
            return redirect('admin');
        }
    }
}
