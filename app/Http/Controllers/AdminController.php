<?php

namespace App\Http\Controllers;

use App\Queue;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function index()
    {
        $queues = Queue::all();
        return view('admins.index', ['queues' => $queues]);
    }
}
