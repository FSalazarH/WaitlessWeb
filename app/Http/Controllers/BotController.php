<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;

class BotController extends Controller
{
    public function store(Request $request)
    {
        $user = User::where('rol', $request->rol)->first();
        if (is_null($user)) {
            $user = new User();
            $user->name = $request->name;
            $user->rol = $request->rol;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->status = 1;
            $user->save();
        }
        
        return redirect()->route('bots.register', ['success' => 'true']);
    }
}
