<?php

namespace App\Http\Controllers;
use App\Queue;
use App\Admin;

use Illuminate\Support\Facades\Auth;

use App\Http\Requests;

class SuperAdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:super_admin');
    }
    
    public function index()
    {
        $queues = Queue::all();
        $admins = Admin::all();
        return view('super-admins.index', [
            'queues' => $queues,
            'admins' => $admins
        ]);
    }

}
