<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/




Route::get('/', function() {
    return redirect('chooseQueue');
});


    Route::get('/liveQueue', [
        'as' => 'liveQueue',
        'uses' => function () {
            return view('liveQueue');
        }
    ]);

    Route::get('/inQueue', [
        'as' => 'inQueue',
        'uses' => function () {
            return view('inQueue');
        }
    ]);

    Route::get('/checkQueue', [
        'as' => 'checkQueue',
        'uses' => function () {
            return view('checkQueue');
        }
    ]);

      Route::get('/index', [
        'as' => 'index',
        'uses' => function () {
            return view('index');
        }
    ]);


/* login */
Route::get('/admin/login', 'AdminAuth\AuthController@showLoginForm');
Route::post('admin/login', 'AdminAuth\AuthController@login');

Route::get('/super-admin/login', 'SuperAdminAuth\AuthController@showLoginForm');
Route::post('/super-admin/login', 'SuperAdminAuth\AuthController@login');

    Route::get('/chooseQueue', [
        'as' => 'chooseQueue',
        'uses' => function () {
            return view('chooseQueue');
        }
    ]);

    
    Route::post('/store', [
        'as' => 'bots.store',
        'uses' => 'BotController@store'
    ]);


/* Admin access */
Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
   Route::get('/', 'AdminController@index');
   Route::get('/logout', 'AdminAuth\AuthController@logout');
   
   Route::resource('queues', 'QueueController', ['except' => [
       'index', 'show'
    ]]);
});

/* Super Admin access */
Route::group(
    [
        'prefix' => 'super-admin',
        'middleware' => 'super_admin'
    ],
    function () {
        Route::get('/', 'SuperAdminController@index');
        Route::get('/logout', 'SuperAdminAuth\AuthController@logout');
        Route::resource('queues', 'QueueController', ['except' => [
            'index', 'show'
        ]]);
    }
);




