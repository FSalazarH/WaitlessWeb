<?php

use Illuminate\Database\Seeder;

class TestUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->delete();
        DB::table('super_admins')->delete();
        
        DB::table('admins')->insert([
            'name' => 'Webmaster',
            'email' => 'foo@bar.com',            
            'password'=> bcrypt("123456"),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        
        DB::table('super_admins')->insert([
            'name' => 'Webmaster Super',
            'email' => 'foo2@bar.com',            
            'password'=> bcrypt("123456"),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
