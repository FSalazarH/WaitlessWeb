<?php

use Illuminate\Database\Seeder;

class QueuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('queues')->delete();
        
        DB::table('queues')->insert([
            'name' => 'Renovación de Becas y Créditos',
            'current_number' => 1,
            'last_number' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        
        DB::table('queues')->insert([
            'name' => 'Gratuidad de Educación',
            'current_number' => 1,
            'last_number' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
