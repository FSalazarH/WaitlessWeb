<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttentionStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attention_status', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('attention_number_id')->unsigned();
           $table->integer('code');
           $table->timestamps();
           
           $table->foreign('attention_number_id')
                ->references('id')->on('attention_numbers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attention_status');
    }
}
