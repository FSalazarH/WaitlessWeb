<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttentionNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attention_numbers', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('queue_id')->unsigned();
           $table->integer('value');
           $table->timestamps();
           
           $table->foreign('queue_id')
                ->references('id')->on('queues')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attention_numbers');
    }
}
